﻿using System;
using System.Collections.Generic;
using System.Linq;
using model;
using model.hall;
using model.show;
using model.ticket;

namespace domain
{
    public class ShowStore : IShowStore
    {
        public readonly List<Show> _shows = new List<Show>();
        public readonly List<TicketTransactionEvent> _events = new List<TicketTransactionEvent>();

        public void AddShows(List<Show> shows)
        {
            _shows.AddRange(shows);
        }

        public bool BuyTicket(Guid showGuid, DateTime date)
        {
            var show = GetShowBy(showGuid, date);

            if (show.TicketsAvailable == show.TicketsLeft) return false;

            _events.Add(new TicketTransactionEvent
                            {
                                ShowGuid = showGuid,
                                ShowTime = date,
                                TransactionType = TransactionType.Sale,
                            });

            return true;
        }

        public ShowModel[] GetAll() =>
            _shows.Select(x => ShowModel.From(x)).ToArray();

        public ShowModel[] GetShowsBy(DateTime date) =>
            _shows.Where(x => x.CanSell(date))
                  .Select(x => ShowModel.From(x,
                                              x.Price(date),
                                              Hall.GetAvailableTickets(x, date),
                                              _events.Count(y => y.ShowGuid == x.Guid && y.SellTime == date)))
                  .ToArray();
        
        public ShowModel GetShowBy(Guid showGuid, DateTime date) =>
            _shows.Where(x => x.Guid == showGuid
                              && x.CanSell(date))
                  .Select(x => ShowModel.From(x,
                                              x.Price(date),
                                              Hall.GetAvailableTickets(x, date),
                                              _events.Count(y => y.ShowGuid == x.Guid && y.SellTime == date)))
                  .FirstOrDefault();
    }
}
