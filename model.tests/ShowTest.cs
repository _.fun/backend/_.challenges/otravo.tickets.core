using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using model.show;

namespace model.tests
{
    [TestClass]
    public class ShowTest
    {
        [TestMethod]
        public void PriceMusical()
        {
            // arrange
            var show = new Show
            {
                StartDate = DateTime.Now,
                Genre = Genre.Musical,
            };

            // act
            var result = show.Price(show.StartDate);

            // assert
            Assert.AreEqual(70.0m, result);
        }

        [TestMethod]
        public void PriceComedy()
        {
            // arrange
            var show = new Show
            {
                StartDate = DateTime.Now,
                Genre = Genre.Comedy,
            };

            // act
            var result = show.Price(show.StartDate);

            // assert
            Assert.AreEqual(50.0m, result);
        }

        [TestMethod]
        public void PriceDrama()
        {
            // arrange
            var show = new Show
            {
                StartDate = DateTime.Now,
                Genre = Genre.Drama,
            };

            // act
            var result = show.Price(show.StartDate);

            // assert
            Assert.AreEqual(40.0m, result);
        }

        [TestMethod]
        public void PriceDiscount()
        {
            // arrange
            var show = new Show
            {
                StartDate = DateTime.Now,
                Genre = Genre.Musical,
            };

            // act
            var result = show.Price(show.StartDate.AddDays(Show.DiscountStartInDays));

            // assert
            Assert.AreEqual(56.0m, result);
        }

        [TestMethod]
        public void IsOnSale()
        {
            // arrange
            var show = new Show
            {
                StartDate = DateTime.Now,
            };

            // act
            var result = show.IsOnSale(show.StartDate.AddDays(Show.DiscountStartInDays));

            // assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CanSell()
        {
            // arrange
            var show = new Show
            {
                StartDate = DateTime.Now,
            };

            // act
            var result = show.CanSell(show.StartDate);

            // assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CannotSellBefore()
        {
            // arrange
            var show = new Show
            {
                StartDate = DateTime.Now,
            };

            // act
            var result = show.CanSell(show.StartDate.AddDays(-(Show.SaleStartInDays + 1)));

            // assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void CannotSellAfter()
        {
            // arrange
            var show = new Show
            {
                StartDate = DateTime.Now,
            };

            // act
            var result = show.CanSell(show.EndDate.AddDays(1));

            // assert
            Assert.IsFalse(result);
        }
    }
}
