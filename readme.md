# Otravo challenge

## Setup

### Easy way

Install `visual studio community edition` for Windows or OSX. Run application.

### Not so easy way

[Installation](https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x)

```shell
cd web.api
dontnet run
```

### Docker

I've added `docker` auto configuration by `visualstudio`. The `csv` file had to be configured as `content` and `always copy`.

```shell
docker-compose up
```

The api will be accesable by `http://localhost/api/shows`

## Use

To get all data use `api/shows`.

To get for specfic date use `api/shows/yyyy-MM-dd`. Fro example `api/shows/2018-05-25`.