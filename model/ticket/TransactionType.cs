﻿using System;

namespace model.ticket
{
    public enum TransactionType
    {
        Sale,
        // not taking into account yet, just as understanding that it's possible
        Return
    }
}
