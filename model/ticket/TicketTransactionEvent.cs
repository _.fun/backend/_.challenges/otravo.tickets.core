﻿using System;

namespace model.ticket
{
    public class TicketTransactionEvent
    {
        public Guid Guid { get; set; } = new Guid();

        public Guid ShowGuid { get; set; }

        public DateTime ShowTime { get; set; }

        public DateTime SellTime { get; set; } = DateTime.UtcNow;

        public TransactionType TransactionType { get; set; } = TransactionType.Sale;
    }
}
