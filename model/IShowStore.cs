﻿using System;
using System.Collections.Generic;
using model.show;

namespace model
{
    public interface IShowStore
    {
        void AddShows(List<Show> shows);

        ShowModel[] GetAll();

        ShowModel[] GetShowsBy(DateTime date);

        ShowModel GetShowBy(Guid showGuid, DateTime date);

        bool BuyTicket(Guid showGuid, DateTime date);
    }
}
