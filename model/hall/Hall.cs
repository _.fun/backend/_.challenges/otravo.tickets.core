﻿using System;
using model.show;

namespace model.hall
{
    // better to move everything to normal structure with schedule
    public static class Hall
    {
        public const int NumberInBigHall = 200;
        public const int NumberInSmallHall = 100;

        public const int MoveToSmallHallBeforeDays = 40;

        public static int GetAvailableTickets(Show show, DateTime date)
        {
            switch(GetHallType(show, date))
            {
                case HallType.Big: return NumberInBigHall;
                case HallType.Small: return NumberInSmallHall;
                default: return 0;
            }
        }

        public static HallType GetHallType(Show show, DateTime date) =>
            show.EndDate.Subtract(date).TotalDays <= MoveToSmallHallBeforeDays
                ? HallType.Small
                : HallType.Big;
    }
}
