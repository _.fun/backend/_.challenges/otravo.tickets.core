﻿using System;
using System.Collections.Generic;
using model.ticket;

namespace model.show
{
    public class Show
    {
        // better to have it in setting somewhere
        // or even better is to have flexible rules with schedule and other things
        // everything is hardcoded, tis sad
        public const int SaleStartInDays = 25;
        public const int ShowRunsForDays = 100;
        public const int DiscountStartInDays = 80;
        public const decimal DiscountInProcentage = 20;

        public Guid Guid { get; set; } = new Guid();

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate => StartDate.AddDays(ShowRunsForDays);

        public Genre Genre { get; set; }

        /// <summary>
        /// Price which is calculated by using date for discount
        /// </summary>
        /// <param name="date">date for discount calculation</param>
        /// <returns>calculated price</returns>
        public decimal Price(DateTime date)
        {
            var result = .0m;

            // no need to calculate price for not sellable product
            if (!CanSell(date)) return result;

            switch(Genre)
            {
                case Genre.Musical: result = 70.0m; break;
                case Genre.Comedy:  result = 50.0m; break;
                case Genre.Drama:   result = 40.0m; break;
                default: break;
            }

            // apply discount by using date
            result = ApplyDiscounted(result, DiscountInProcentage, date);

            return result;
        }

        /// <summary>
        /// Applies the discounted.
        /// </summary>
        /// <param name="price">Price.</param>
        /// <param name="discount">Discount.</param>
        /// <param name="date">Date.</param>
        /// <returns>The discounted price.</returns>
        public decimal ApplyDiscounted(decimal price,
                                       decimal discount,
                                       DateTime date) =>
            IsOnSale(date)
              ? price - ((price / 100) * discount)
              : price;
        
        /// <summary>
        /// Check that show is on sale
        /// </summary>
        /// <param name="date">Date.</param>
        /// <returns><c>true</c>, if on sale, <c>false</c> otherwise.</returns>
        public bool IsOnSale(DateTime date) =>
            // should be before shows end
            EndDate > date
            // days before end should be in the limit
            && EndDate.Subtract(date).TotalDays <= DiscountStartInDays;
        
        /// <summary>
        /// Can sell show tickets
        /// </summary>
        /// <param name="date">Date.</param>
        /// <returns><c>true</c>, can sell, <c>false</c> otherwise.</returns>
        public bool CanSell(DateTime date) =>
            // after show sale start
            StartDate.AddDays(-SaleStartInDays) <= date
            // before shows end                     
            && EndDate > date;
    }
}
