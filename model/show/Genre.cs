﻿using System;
using System.ComponentModel;

namespace model.show
{
    public enum Genre
    {
        [Description("Musical")]
        Musical,

        [Description("Comedy")]
        Comedy,

        [Description("Drama")]
        Drama,
    }
}
