﻿using System;
namespace model.show
{
    public class ShowModel
    {
        public Guid Guid { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public Genre Genre { get; set; }

        public decimal Price { get; set; }

        public int TicketsAvailable { get; set; }

        public int TicketsLeft { get; set; }

        public static ShowModel From(Show show,
                                     decimal price = .0m,
                                     int available = 0, // defaults
                                     int bought = 0) =>
            new ShowModel
                {
                    Guid = show.Guid,
                    Name = show.Name,
                    StartDate = show.StartDate,
                    EndDate = show.EndDate,
                    Genre = show.Genre,
                    Price = price,
                    TicketsAvailable = available,
                    TicketsLeft = available - bought,
                };
    }
}
