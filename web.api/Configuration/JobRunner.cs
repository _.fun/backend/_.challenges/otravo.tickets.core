﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;
using domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using model;
using model.show;

namespace web.api.Configuration
{
    public static class JobRunner
    {
        public static void RunCSVParsing(this IApplicationBuilder app)
        {
            var showStore = app.ApplicationServices.GetRequiredService<IShowStore>();
            var _fileProvider = app.ApplicationServices.GetRequiredService<IFileProvider>();

            var shows = new List<Show>();

            var reader = _fileProvider.GetFileInfo("shows.csv").CreateReadStream();
            var csv = new CsvReader(new StreamReader(reader));

            // Don't forget to read the data before getting it.
            while (csv.Read())
            {
                // Gets field by position returning int
                var name = csv.GetField<string>(0);
                var date = csv.GetField<string>(1);
                var genre = csv.GetField<string>(2);

                shows.Add(new Show
                {
                    Name = name,
                    StartDate = DateTime.TryParseExact(date,
                                                       "yyyy-MM-dd",
                                                       CultureInfo.InvariantCulture,
                                                       DateTimeStyles.None,
                                                       out var dateParsed)
                                    ? dateParsed
                                    : DateTime.Now,
                    Genre = Enum.TryParse(typeof(Genre), genre, true, out var genreParsed)
                                ? (Genre)genreParsed
                                : Genre.Musical,
                });
            }

            showStore.AddShows(shows);
        }
    }
}
