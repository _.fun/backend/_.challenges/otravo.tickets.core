﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using model;
using web.api.Models;

namespace web.api.Controllers
{
    [Route("api/[controller]")]
    public class ShowsController : Controller
    {
        private readonly IShowStore _showStore;

        public ShowsController(IShowStore showStore)
        {
            _showStore = showStore;
        }

        [HttpGet]
        public IEnumerable<ShowGroupModel> Get() =>
            ShowGroupModel.From(_showStore.GetAll());

        [HttpGet("{date}")]
        public IEnumerable<ShowGroupModel> Get(string date) =>
            ShowGroupModel.From(_showStore.GetShowsBy(DateTime.TryParseExact(date,
                                                                             "yyyy-MM-dd",
                                                                              CultureInfo.InvariantCulture,
                                                                              DateTimeStyles.None,
                                                                              out var dateParsed)
                                                        ? dateParsed
                                                        : DateTime.Now));

        // add buy api later
    }
}
