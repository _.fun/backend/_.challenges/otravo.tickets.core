﻿using System.Linq;
using model.show;

namespace web.api.Models
{
    public class ShowGroupModel
    {
        public Genre Genre { get; set; }

        public ShowModel[] Shows { get; set; }

        public static ShowGroupModel[] From(ShowModel[] shows) =>
        shows.GroupBy(x => x.Genre)
             .Select(x => new ShowGroupModel
                              {
                                  Genre = x.Key,
                                  Shows = x.ToArray()
                              })
             .ToArray();
    }
}
